# Docker-compose settings
#### on base of wordpress image with localdatabase & basic adminer

You need to create .env file in root folder on base of example.
**By default:**
PROJECT_WP_URL - localhost
PROJECT_WP_PORT - 8080

**To run project:**
```
docker-compose up -d
```

**To stop project (in console in this folder):**
```
docker-compose down
```

You can run the project. Docker will create wp folder with all defauld wp structure. 
Then you can paste the theme/plugins/uploads from your project or leave it if you want to start new project.

To go to adminer go in browser to localhost:6080
